from django.shortcuts import render
from django.utils import timezone

from .models import Quiz, QuizTaker


def home(request):
    return render(request, 'quizzes/home.html')


def check_expiration(quiz):
    now = timezone.now()

    if quiz.expiration_date < now:
        quiz.delete()


def index(request):
    quiz_list = Quiz.objects.all().order_by('created_date')
    for quiz in quiz_list:
        check_expiration(quiz)

    sessions = QuizTaker.objects.filter(user=request.user.id).order_by('-created')[0:10]

    context = {'quiz_list': quiz_list,
               'sessions': sessions}

    return render(request, 'quizzes/index.html', context)


def quiz_form(request, pk):
    quiz = Quiz.objects.get(pk=pk)
    questions_list = quiz.question_set.all()
    context = {'quiz': quiz, 'questions_list': questions_list}
    return render(request, 'quizzes/quiz.html', context)


def results(request):
    # Initialize score and container of answers
    score = 0
    answers = []
    correct_answers = []
    indicators = []

    quiz_chosen = Quiz.objects.get(title=request.POST.get("quiz_title"))
    questions_list = quiz_chosen.question_set.all()

    for question in questions_list:
        answers.append(request.POST.get(question.question_text))
        correct_answers.append(question.choice_set.get(correct=True).label)

    for answer in answers:

        if answer in correct_answers:
            score += 1
            indicators.append('Your answer is: ' + answer + '. CORRECT!')
        else:
            indicators.append('Your answer is: ' + answer + '. INCORRECT!')

    total = len(answers)

    QuizTaker(user=request.user, quiz=quiz_chosen, score=score).save()

    context = {'quiz_chosen': quiz_chosen,
               'questions_list': questions_list,
               'answers': answers,
               'correct_answers': correct_answers,
               'indicators': indicators,
               'score': score,
               'total': total,
               'percent': int(score / total * 100)}

    return render(request, 'quizzes/results.html', context)
