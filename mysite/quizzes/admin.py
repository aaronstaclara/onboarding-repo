from django.contrib import admin

from .models import Quiz, Question, Choice, QuizTaker


class QuestionInline(admin.TabularInline):
    model = Question
    extra = 4

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 4

class QuestionAdmin(admin.ModelAdmin):
    inlines = [ChoiceInline]
    list_display = ('question_text','quiz')

class ChoiceAdmin(admin.ModelAdmin):
    pass

class QuizAdmin(admin.ModelAdmin):
    inlines = [QuestionInline]

admin.site.register(Question, QuestionAdmin)
admin.site.register(Quiz)
admin.site.register(QuizTaker)