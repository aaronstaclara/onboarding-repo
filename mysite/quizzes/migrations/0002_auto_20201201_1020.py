# Generated by Django 3.1.3 on 2020-12-01 02:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quizzes', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='question',
        ),
        migrations.AddField(
            model_name='question',
            name='question_text',
            field=models.TextField(null=True),
        ),
    ]
