from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='quizzes-home'),
    path('quiz/<int:pk>', views.quiz_form, name='quiz_form'),
    path('quiz/results', views.results, name='results')
]
